package epam.controller;

import java.io.File;

public class DirectoryTree implements Runnable {
    public void run() {
        printTree(new File("src"));
    }

    public static void printTree(File dir) {

        File listFile[] = dir.listFiles();
        if (listFile != null) {
            for (int i = 0; i < listFile.length; i++) {
                if (listFile[i].isDirectory()) {
                    System.out.println("|\t\t");
                    printTree(listFile[i]);
                } else {
                    System.out.println("+---" + listFile[i].getName().toString());
                }
            }
        }
    }
}
