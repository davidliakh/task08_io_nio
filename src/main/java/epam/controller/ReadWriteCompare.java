package epam.controller;

import epam.model.Runnable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class ReadWriteCompare implements Runnable {
    private static final Logger logger = LogManager.getLogger();

    public void readWithoutBuffer() {
        long startTime = System.currentTimeMillis();
        try (FileReader fileReader = new FileReader("10mb.txt")) {
            int ch = fileReader.read();
            while (ch != -1) {
                ch = fileReader.read();
            }
        } catch (FileNotFoundException e) {
            logger.error(e);
        } catch (IOException e) {
            logger.error(e);
        }
        long endTime = System.currentTimeMillis();
        long fullTime = endTime - startTime;
        System.out.println("Execution time(sec) of reading without buffer = " + (double) fullTime * 0.001);
    }

    public String linesFromFile() {
        StringBuilder text = new StringBuilder();
        try (FileReader fileReader = new FileReader("10mb.txt")) {
            int ch = fileReader.read();
            while (ch != -1) {
                text.append((char) ch);
            }
        } catch (FileNotFoundException e) {
            logger.error(e);
        } catch (IOException e) {
            logger.error(e);
        }
        return text.toString();
    }

    public void writeWithoutBuffer() {
        long startTime = System.currentTimeMillis();
        try (FileWriter fileWriter = new FileWriter("text.txt")) {
            fileWriter.write(linesFromFile());
        } catch (IOException e) {
            logger.error(e);
        } catch (OutOfMemoryError e) {
            logger.error(e);
        }
        long endTime = System.currentTimeMillis();
        long fullTime = endTime - startTime;
        System.out.println("Execution time(sec) of writing without buffer = " + (double) fullTime * 0.001);
    }

    public void readWithBuffer() {
        long startTime = System.currentTimeMillis();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("10mb.txt"))) {
            String line = bufferedReader.readLine();
            while (line != null) {
                line = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            logger.error(e);
        } catch (IOException e) {
            logger.error(e);
        }
        long endTime = System.currentTimeMillis();
        long fullTime = endTime - startTime;
        System.out.println("Execution time(sec) of reading with buffer = " + (double) fullTime * 0.001);
    }

    public void writeWithBuffer() {
        long startTime = System.currentTimeMillis();
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("text.txt"))) {
            bufferedWriter.write(linesFromFile());
        } catch (IOException e) {
            logger.error(e);
        }catch (OutOfMemoryError e) {
            logger.error(e);
        }
        long endTime = System.currentTimeMillis();
        long fullTime = endTime - startTime;
        System.out.println("Execution time(sec) of writing with buffer = " + (double) fullTime * 0.001);
    }

    @Override
    public void run() {
        readWithoutBuffer();
        writeWithoutBuffer();
        readWithBuffer();
        writeWithBuffer();
    }
}
