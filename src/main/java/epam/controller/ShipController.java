package epam.controller;

import epam.model.Droids;
import epam.model.Runnable;
import epam.model.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class ShipController implements Runnable {
    private static final Logger logger = LogManager.getLogger();

    public void serializeShip() {
        Ship ship = new Ship(13, 554.12, 12, new Droids("Droid_1", 666.6), 3);
        try {
            FileOutputStream fileOut =
                    new FileOutputStream("Ship.txt");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(ship);
            out.close();
            fileOut.close();
            System.out.println("Serializetion done");
        } catch (IOException I) {
            logger.error(I);
        }
    }

    public void deserializeShip() {
        Ship ship = null;
        try {
            FileInputStream fileIn = new FileInputStream("Ship.txt");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            ship = (Ship) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            logger.error(i);
        } catch (ClassNotFoundException e) {
            logger.error(e);
            return;
        }
        System.out.println(ship);
    }

    @Override
    public void run() {
        serializeShip();
        deserializeShip();
    }
}
