package epam.controller;

import java.nio.InvalidMarkException;

public class SomeBuffer {

    private final static int DEFAULT_BUFFER_SIZE = 8192;
    private byte[] bytesBuffer;
    private int mark = -1;
    private int position = 0;
    private int limit;
    private int capacity;

    public SomeBuffer() {
        this.bytesBuffer = new byte[DEFAULT_BUFFER_SIZE];
        this.capacity = DEFAULT_BUFFER_SIZE;
    }

    public SomeBuffer(int capacity) {
        this.bytesBuffer = new byte[capacity];
        this.capacity = capacity;
    }

    public int capacity() {
        return capacity;
    }

    public SomeBuffer clear() {
        position = 0;
        limit = capacity;
        mark = -1;
        return this;
    }

    public SomeBuffer flip() {
        limit = position;
        position = 0;
        mark = -1;
        return this;
    }

    public SomeBuffer rewind() {
        position = 0;
        mark = -1;
        return this;
    }

    public int remaining() {
        return limit - position;
    }

    public final boolean hasRemaining() {
        return position < limit;
    }

    public int limit() {
        return limit;
    }

    public SomeBuffer limit(int newLimit) {
        if (newLimit < capacity | newLimit > 0) {
            limit = newLimit;
            if (position > limit) {
                position = limit;
            }
            if (mark > limit) {
                mark = -1;
            }
        }
        return this;
    }

    public int position() {
        return position;
    }

    public SomeBuffer position(int newPosition) {
        if (newPosition < limit | newPosition > 0) {
            position = newPosition;
        }
        if (mark > position) {
            mark = -1;
        }
        return this;
    }

    public SomeBuffer mark() {
        mark = position;
        return this;
    }

    public SomeBuffer reset() {
        if (mark < 0) {
            throw new InvalidMarkException();
        }
        position = mark;
        return this;
    }
}
