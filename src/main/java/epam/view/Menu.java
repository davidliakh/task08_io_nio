package epam.view;

import epam.controller.DirectoryTree;
import epam.controller.ReadComments;
import epam.controller.ReadWriteCompare;
import epam.controller.ShipController;

import java.util.ArrayList;
import java.util.Scanner;

public class Menu {
    private static Scanner scanner = new Scanner(System.in);
    private static ShipController ship = new ShipController();
    private static ReadWriteCompare readWrite = new ReadWriteCompare();
    private static ReadComments readComments = new ReadComments();
    private static DirectoryTree tree = new DirectoryTree();
    private static Menu menu = new Menu();

    public static void main(String[] args) {
        menu.printMenu();
    }

    public ArrayList menuList() {
        ArrayList<String> list = new ArrayList<>();
        list.add("Ship with droids");
        list.add("Read, write with buffer and without Buffer");
        list.add("Read comments from java file");
        list.add("Print tree of directory");
        return list;
    }

    public void printMenu() {
        ArrayList<String> list = new ArrayList<>(menuList());
        System.out.println("Input number for task :");
        for (int i = 1; i < 5; i++) {
            System.out.println("input " + i + " for " + list.get(i - 1));
        }
        int choice = scanner.nextInt();
        switch (choice) {
            case 1:
                ship.run();
                menu.printMenu();
                break;
            case 2:
                readWrite.run();
                menu.printMenu();
                break;
            case 3:
                readComments.run();
                menu.printMenu();
                break;
            case 4:
                tree.run();
                menu.printMenu();
                break;
            default:
                System.out.println("Input correct Number");
                menu.printMenu();
        }
    }

}
