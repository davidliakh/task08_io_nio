package epam.model;

import java.io.Serializable;

public class Ship implements Serializable {
    private int size;
    private double speed;
    private transient int id;
    private Droids droid;
    private double capacity;

    public Ship(int size, double speed, int id, Droids droid, double capacity) {
        this.size = size;
        this.speed = speed;
        this.id = id;
        this.droid = droid;
        this.capacity = capacity;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Droids getDroid() {
        return droid;
    }

    public void setDroid(Droids droid) {
        this.droid = droid;
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        return "Ship details: Size =" + size +
                ", Speed =" + speed +
                ", id =" + id +
                ", Droid =" + droid +
                ", capacity = " + capacity;
    }
}
